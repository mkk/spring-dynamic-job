## spring-dynamic-job

> SpringBoot与Quartz整合示例，有固定Job与动态Job实现示例，并提供相关工具类。

项目用Maven管理, SpringBoot工程


使用的技术与版本号
<ol>
 <li>OpenJDK (17)</li>
 <li>SpringBoot (3.1.5)</li>
 <li>Quartz (2.3.2)</li>
 <li>mysql-connector-j (8.0.33)</li>
 <li>Maven (3.6.0)</li>
</ol>
<hr/>

### 使用说明

<ol>
    <li>
        Check-out代码, 使用IDE(如IDEA) 用Maven工程方式打开
    </li>
    <li>
        修改<code>application.properties</code>文件中数据库连接信息,并创建数据库(默认数据为名sdj), 
        运行<code>others/quartz_mysql_innodb.sql</code>文件初始化数据库
    </li>
    <li>
        直接运行启动类 <code>SDJApplication.java</code>
    </li>    
    <li>
        启动成功可在浏览器访问 http://localhost:8080 可添加或删除动态的Job, 也可在控制台中看到固定Job的输出日志
    </li>
</ol>

<p>
    项目的核心对象为: <code>DynamicSchedulerFactory</code>
    <br/>
    该项目已在 <a href="https://gitee.com/mkk/HeartBeat">HeartBeat</a> 项目中实际使用,
    更多运用案例可查看该项目.
</p>

<p>
    动态的Job定义类: <code>DynamicJob</code>, 在动态操作Job时的类必须是该类的子类(参考<code>TestDynamicJob</code>)
    <br/>
    固定执行的Job示例类: <code>TestFixedJobDetailBean</code>, 启动时加载,固定执行.
    <br/>
    更多的操作请参考类: <code>TestServiceImpl</code>
</p>

<p>如果不使用数据库(内存中存储job信息), 取消 dataSource 的配置与引用项</p>

<hr/>

### 详细讲解

定时任务分两类, 固定的(fixed)与动态的(dynamic)

一.固定的    - 即指固定时间执行(如每天凌晨2点, 或每隔30秒执行)

1.参考TestFixedJobDetailBean类写具体的实现

2.在 SDJConfiguration.java 中配置 job (参照注释部分)<br/>
  1). 配置 JobDetail (参考 fixedJobDetail)<br/>
  2).配置 Trigger, 参考 fixedTrigger (注意 cronExpression 的值配置)<br/>
      对于 cronExpression 的写法, 请参考 http://blog.csdn.net/caiwenfeng_for_23/article/details/17004213<br/>
  3).将 JobDetail与Trigger 添加到 schedulerFactory 中 (参考 schedulerFactory 配置)<br/>

提示: 对于手动编写的 cronExpression 可使用 CronExpression.java 类 进行校验测试


二.动态的    - 即在系统运行中动态添加的job(如 用户添加的 用户同步定时任务)

1.参考 TestDynamicJob 类写具体的实现

2.在需要使用时 创建对应的 DynamicJob 对象,参考如下:

    //创建 一个动态的JOB, 测试用
    private DynamicJob createDynamicJob() {
        return new DynamicJob("test-")
                //动态定时任务的 cron,  每20秒执行一次
                .cronExpression("0/20 * * * * ?")
                .target(TestDynamicJob.class);
    }

提示: Job name 要具体, cronExpression 一般根据时间对象(Date)来生成, target对第1步新建的对象

3.如果在Job执行时有参数, 加上对应的参数名与参数值(可多个), 如下:<br/>

        DynamicJob dynamicJob = createDynamicJob();
        dynamicJob.addJobData("mailGuid", UUID.randomUUID().toString());//transfer parameter

这样在Job实现类中可通过 context 来获取,如下:<br/>
<pre>
final String mailGuid = context.getMergedJobDataMap().getString("mailGuid");
</pre>
4.向 DynamicSchedulerFactory 中注册job, 如下:<br/>
<pre>
 DynamicSchedulerFactory.registerJob(dynamicJob);
</pre>

三.动态的但立即执行    - 且只执行一次(适用于将同步的操作异步化)

和二的区别在于其第2步创建DynamicJob 对象时设置startNow 为true(不设置cronExpression)
<pre>
DynamicJob dynamicJob = new DynamicJob("test-job_start-now")
            .startNow(true)
            .target(TestDynamicJob.class);
</pre>

<hr/>

### 项目动态

1. 2014-12-26 创建项目，第一次提交代码
2. 2014-12-27 发表相关介绍文章，https://blog.csdn.net/monkeyking1987/article/details/42173277
3. 2015-06-15 发布v0.1版本，https://gitee.com/mkk/spring-dynamic-job/tree/0.1/
4. 2017-05-22 发布v1.0版本，https://gitee.com/mkk/spring-dynamic-job/tree/1.0/
5. 2023-09-15 发布v1.1版本，大更新与升级，详见 https://www.oschina.net/news/258217/spring-dynamic-job-1-1-released
6. 2024-04-18 开始开发v1.2.0版本，增加job startNow 功能。

<hr/>

### 帮助与改进

<ol>
<li>
<p>
 与该项目相关的博客请访问 <a target="_blank" href="http://blog.csdn.net/monkeyking1987/article/details/42173277">http://blog.csdn.net/monkeyking1987/article/details/42173277</a>
</p>
</li>
<li>
<p>
 若没有找到解决办法的,
 欢迎发邮件到<a href="mailto:shengzhao@shengzhaoli.com">shengzhao@shengzhaoli.com</a>一起讨论.
</p>
</li>

<li>
<p>
 如果在使用项目的过程中发现任何的BUG或者更好的提议, 建议将其提交到项目的 <a href="https://gitee.com/mkk/spring-dynamic-job/issues">Issues</a> 中,
 我会一直关注并不断改进项目.
</p>
</li>
</ol>

<hr/>
<p>
 关注更多我的开源项目请访问 <a href="https://andaily.com/my_projects.html">https://andaily.com/my_projects.html</a>
</p>
