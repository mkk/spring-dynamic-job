package com.andaily;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 2023/9/14 16:40
 *
 * @author Shengzhao Li
 * @since 1.1
 */
@SpringBootTest
@Profile("test")
class SDJApplicationTest {


    @Test
    void contextLoads() {
    }

}