package com.andaily.service.impl;

import org.junit.jupiter.api.Test;
import org.quartz.CronExpression;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 2023/9/14 17:57
 *
 * @author Shengzhao Li
 * @since 1.1
 */
class TestServiceImplTest {


    @Test
    void cronExpression() throws Exception {

        CronExpression cronExpression = new CronExpression("0/30 * * * * ?");
        Date nextValidTimeAfter = cronExpression.getNextValidTimeAfter(new Date());
        assertNotNull(nextValidTimeAfter);

    }

}