package com.andaily.web;

import com.andaily.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;

/**
 * @author Shengzhao Li
 * @since 0.1
 */
@Controller
public class IndexController {


    @Autowired
    private TestService testService;

    @GetMapping(value = {"/", "index.xhtm"})
    public String index(Model model) {
        model.addAttribute("date", new Date());
        return "index";
    }

    /**
     * add  new job
     */
    @GetMapping("add_job.xhtm")
    public String addJob(boolean startNow, RedirectAttributes model) {
        boolean result = testService.addDynamicJob(startNow);
        model.addAttribute("result", result);
        return "redirect:index.xhtm";
    }

    /**
     * remove  existed job
     */
    @GetMapping("remove_job.xhtm")
    public String removeJob(boolean startNow) {
        testService.removeJob(startNow);
        return "redirect:index.xhtm";
    }
}