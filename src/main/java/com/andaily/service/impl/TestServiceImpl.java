package com.andaily.service.impl;

import com.andaily.service.TestService;
import com.andaily.service.scheduler.TestDynamicJob;
import com.andaily.service.scheduler.dynamic.DynamicJob;
import com.andaily.service.scheduler.dynamic.DynamicSchedulerFactory;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.andaily.service.scheduler.KeyConstants.MAIL_GUID;
import static com.andaily.service.scheduler.KeyConstants.START_NOW;

/**
 * @author Shengzhao Li
 * @since 0.1
 */
@Service("testService")
public class TestServiceImpl implements TestService {

    private static final Logger LOG = LoggerFactory.getLogger(TestServiceImpl.class);


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addDynamicJob(boolean startNow) {
        DynamicJob dynamicJob = createDynamicJob(startNow);
        //transfer parameter
        dynamicJob.addJobData(MAIL_GUID, UUID.randomUUID().toString());
        dynamicJob.addJobData(START_NOW, startNow);

        try {
            boolean ok = DynamicSchedulerFactory.registerJob(dynamicJob);
            LOG.info("Register DynamicJob [{}] -> {}", dynamicJob, ok);
        } catch (SchedulerException e) {
            throw new IllegalStateException(e);
        }

        return true;
    }


    /**
     * 删除一个JOB
     */
    @Override
    public void removeJob(boolean startNow) {
        final DynamicJob dynamicJob = createDynamicJob(startNow);
        try {
            final boolean result = DynamicSchedulerFactory.removeJob(dynamicJob);
            LOG.info("Remove DynamicJob [{}] result: {}", dynamicJob, result);
        } catch (SchedulerException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * 创建 一个动态的JOB, 测试用
     */
    private DynamicJob createDynamicJob(boolean startNow) {
        if (startNow) {
            return new DynamicJob("test-job_start-now")
                    .startNow(true)
                    .target(TestDynamicJob.class);
        } else {
            return new DynamicJob("test-job_cron")
                    //动态定时任务的 cron,  每20秒执行一次
                    .cronExpression("0/20 * * * * ?")
                    .target(TestDynamicJob.class);
        }
    }
}