package com.andaily.service.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

import static com.andaily.service.scheduler.KeyConstants.MAIL_GUID;
import static com.andaily.service.scheduler.KeyConstants.START_NOW;

/**
 * Just for testing
 *
 * @author Shengzhao Li
 */
public class TestDynamicJob implements Job {


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        final String mailGuid = context.getMergedJobDataMap().getString(MAIL_GUID);
        final boolean startNow = context.getMergedJobDataMap().getBoolean(START_NOW);

        System.out.println("[Dynamic-Job]  It is " + new Date() + " now, mailGuid=" + mailGuid);
        if (startNow) {
            System.out.println("startNow is true, only execute one time.");
        }
    }
}