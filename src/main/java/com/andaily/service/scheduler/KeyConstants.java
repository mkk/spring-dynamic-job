package com.andaily.service.scheduler;

/**
 * 2024/4/19 00:33
 * <p>
 * Job中变量名
 *
 * @author Shengzhao Li
 * @since 1.2.0
 */
public interface KeyConstants {


    String MAIL_GUID = "mailGuid";

    String START_NOW = "startNow";
}
