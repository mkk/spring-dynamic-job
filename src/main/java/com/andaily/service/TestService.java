package com.andaily.service;

/**
 * @author Shengzhao Li
 * @since 0.1
 */
public interface TestService {

    /**
     * 添加一个动态的JOB
     *
     * @param startNow true 表示立即执行, false 表示等待触发执行
     */
    boolean addDynamicJob(boolean startNow);

    void removeJob(boolean startNow);
}