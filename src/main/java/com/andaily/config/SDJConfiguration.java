package com.andaily.config;

import com.andaily.service.scheduler.TestFixedJobDetailBean;
import com.andaily.service.scheduler.dynamic.DynamicSchedulerFactory;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;

/**
 * 2023/9/14 15:10
 * <p>
 * Main configuration
 * <p>
 * <p>
 * replaced spring-dynamic-job.xml
 *
 * @author Shengzhao Li
 * @since 1.1
 */
@Configuration
public class SDJConfiguration {


    @Autowired
    private ApplicationContext applicationContext;


    @Value("${spring.quartz.auto-startup:true}")
    private boolean quartzAutoStartup;

    @Value("${spring.quartz.overwrite-existing-jobs:true}")
    private boolean quartzOverwriteExistingJobs;

    @Value("${spring.quartz.wait-for-jobs-to-complete-on-shutdown:true}")
    private boolean quartzWaitForJobsToCompleteOnShutdown;


    @Value("${test.fixed.cron.expression:0/30 * * * * ?}")
    private String testFixedCronExpression;


    /**
     * fixed job config demo
     */
    @Bean
    public JobDetail fixedJobDetail() {
        return JobBuilder.newJob(TestFixedJobDetailBean.class)
                .withIdentity("testFixedJob")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger fixedTrigger() {
        return TriggerBuilder.newTrigger()
                .forJob(fixedJobDetail())
                .withIdentity("testFixedTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(this.testFixedCronExpression))
                .build();
    }


    /**
     * Scheduler init
     */
    @Bean
    public SchedulerFactoryBean schedulerFactory(DataSource dataSource) {
        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
        factoryBean.setAutoStartup(this.quartzAutoStartup);
        factoryBean.setOverwriteExistingJobs(this.quartzOverwriteExistingJobs);
        factoryBean.setWaitForJobsToCompleteOnShutdown(this.quartzWaitForJobsToCompleteOnShutdown);
        factoryBean.setApplicationContext(this.applicationContext);
        factoryBean.setDataSource(dataSource);

        // more jobDetails add here
        JobDetail[] jobDetails = new JobDetail[]{fixedJobDetail()};
        factoryBean.setJobDetails(jobDetails);
        // more triggers add here
        Trigger[] triggers = new Trigger[]{fixedTrigger()};
        factoryBean.setTriggers(triggers);
        return factoryBean;
    }


    /**
     * dynamic scheduler
     */
    @Bean
    public DynamicSchedulerFactory dynamicSchedulerFactory(Scheduler scheduler) {
        DynamicSchedulerFactory dynamicSchedulerFactory = new DynamicSchedulerFactory();
        dynamicSchedulerFactory.setScheduler(scheduler);
        return dynamicSchedulerFactory;
    }

}
