package com.andaily;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 2023/9/14 15:03
 *
 * @author Shengzhao Li
 * @since 1.1
 */
@SpringBootApplication
public class SDJApplication {


    /**
     * Run directly
     */
    public static void main(String[] args) {
        SpringApplication.run(SDJApplication.class, args);
    }


}
